
public class Simple_ATM_Service implements ATM_Service {
	private ATM_Exception.ExceptionTYPE ex_type;
	private ATM_Exception ex = new ATM_Exception(ex_type);
	public boolean checkBalance(Account account, int money) throws ATM_Exception{
		if((account.getBalance()-money)<=0) {
			ex.setExceptionCondition(ex_type.BALANCE_NOT_ENOUGH);
			throw new ATM_Exception(ex.getExceptionCondition());
		}
		else
			return true;
	}
	public boolean isValidAmount(int money) throws ATM_Exception{
		if(money%1000==0)
			return true;
		else {
			ex.setExceptionCondition(ex_type.AMOUNT_INVALID);
			throw new ATM_Exception(ex.getExceptionCondition());
		}
	}
	public void withdraw(Account account, int money){
			try {
				if(isValidAmount(money)&&
				checkBalance(account,money)) {
				account.setBalance(account.getBalance()-money);
				System.out.println("updated balance : "+account.getBalance());
				}
		
			} catch (ATM_Exception e) {
				System.out.println(e.getMessage());
				System.out.println("updated balance : "+account.getBalance());
			}
		
	}
}
