
public class ATM_Exception extends Exception {
	enum ExceptionTYPE {
        BALANCE_NOT_ENOUGH, AMOUNT_INVALID
    }
    private ATM_Exception.ExceptionTYPE exceptionCondition;
 
	public ATM_Exception() {
	}
     
    public ATM_Exception(ATM_Exception.ExceptionTYPE _exceptionCondition) {
        exceptionCondition = _exceptionCondition;
    }
     
    public void setExceptionCondition(ATM_Exception.ExceptionTYPE _exceptionCondition) {
        exceptionCondition = _exceptionCondition;
    }
    
    public ATM_Exception.ExceptionTYPE getExceptionCondition() {
        return exceptionCondition;
    }
 
    public String getMessage() {
        return exceptionCondition.toString();
    }
}
