
public class SimpleArrayList {
	private int _size;
	private Integer[] array;
	public SimpleArrayList(){
		_size=0;
		array=new Integer[100];
	}
	public SimpleArrayList(int size){
		_size=size;
		array=new Integer[100];
		for(int i=0;i<size;i++) array[i]=0;
	}
	public void add(Integer i) {
		array[_size]=i;
		_size++;
	}
	public void clear() {
		for(int i=0;i<_size;i++) array[i]=0;
		_size=0;
	}
	public Integer get(int index) {
		if(index>=_size||index<0) return null;
		else return array[index];
	}
	public Integer set(int index,Integer x) {
		if(index>=_size||index<0) return null;
		else {
			Integer toBeReturn=array[index];
			array[index]=x;
			return toBeReturn;
		}
	}
	public boolean remove(int index) {
		if(index>=_size||index<0||array[index]==null) return false;
		else {
			int i=0;
			for(;i<_size-index;i++)
				array[index+i]=array[index+i+1];
			array[index+i]=(Integer) null;
			_size--;
			return true;
		}
	}
	public int size() {
		return _size;
	}
	public boolean retainAll(SimpleArrayList l) {
		int size=_size;
		for(int i=0;i<_size;i++) {
			int j=0;
			for(;j<l._size;) {
				if(array[i]==l.array[j]) {
					break;
				}j++;
			}
			if(j==l._size) {
				this.remove(i);
				i-=1;
			}
		}
		if(size==_size) return false;
		else return true;
	}
}
