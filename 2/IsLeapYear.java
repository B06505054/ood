/**
 * @author sophia
 * @param year, isLeap
 * This is a program that can define if the input year is leap or not
 */
public class IsLeapYear{
    public boolean determine (int year){
		boolean isLeap=false;// set a param check if the year is leap
        if(year%100!=0) {
            if(year%4==0) isLeap=true;//check if the year is leap
        }
        else {
            if(year%400==0) isLeap=true;//check if the year is leap
        }
        return (isLeap)? true:false;
    }
}


