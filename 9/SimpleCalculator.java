
public class SimpleCalculator {
	private int count=0;
	private double result,num;
	private char operator;
	boolean end;
	public SimpleCalculator() {
		result=0.0;
		count=0;
		end=false;
	}
	public void calResult(String cmd)throws UnknownCmdException {
		double value = 0;
		String[] op_v = null ;
		boolean invalid_v=false;
		if(cmd.length()==0)
			throw new UnknownCmdException("Please enter 1 operator and 1 value separated by 1 space");
		else if(cmd.charAt(0)==' ')
			throw new UnknownCmdException("Please enter 1 operator and 1 value separated by 1 space");
		op_v = cmd.split(" ");
		if(op_v.length!=2)
			throw new UnknownCmdException("Please enter 1 operator and 1 value separated by 1 space");
		try {
			value = Double.parseDouble(op_v[1]);
		} catch (NumberFormatException e) {
			invalid_v = true;
		}
		operator=op_v[0].charAt(0);
		
		 if (!(operator == '+' || operator == '-' || operator == '*' || operator == '/')) {
             if (invalid_v) {
                     throw new UnknownCmdException(
                             "" + operator + " is an unknown operator and " +
                            op_v[1] + " is an unknown value");
             } else {
                     throw new UnknownCmdException(
                             operator + " is an unknown operator");
             }
     } else {
             if (invalid_v) {
                     throw new UnknownCmdException(
                    		 op_v[1] + " is an unknown value");
             }
     }

     switch (operator) {
	     case '+':
		         result += value;
		         break;
	     case '-':
	             result -= value;
	             break;
	     case '*':
	             result *= value;
	             break;
	     case '/':
	    	 	if(value==0) {
	    	 		throw new UnknownCmdException("Can not divide by 0");
	    	 	}
	    	 	else { 
	    	 		result /= value;
	    	 	}
	             break;
	     }
     	 num=value;
	}
	public boolean endCalc(String cmd) {
		if(cmd.equals("r")||cmd.equals("R")) {
			end=true;
			return true;
		}
		else {
			end=false;
			return false;
		}
	}
	public String getMsg() {
		String result_f = String.format("%.2f", result);
		String num_f = String.format("%.2f", num);
		if (end)
             return ("Final result = " + result_f);
		if(count==0) {
			count++;
			return "Calculator is on. Result = "+result_f;
		}
		else if(count==1) {
			count++;
			return "Result "+operator+" "+num_f+" = "+result_f+". New result = "+result_f;
		}
		else return "Result "+operator+" "+num_f+" = "+result_f+". Updated result = "+result_f;
	}
}



/*You will complete at least 3
methods in 𝚂𝚒𝚖𝚙𝚕𝚎𝙲𝚊𝚕𝚌𝚞𝚕𝚊𝚝𝚘𝚛
class:
1. 𝚙𝚞𝚋𝚕𝚒𝚌 𝚟𝚘𝚒𝚍 𝚌𝚊𝚕𝚁𝚎𝚜𝚞𝚕𝚝(𝚂𝚝𝚛𝚒𝚗𝚐 𝚌𝚖𝚍) 𝚝𝚑𝚛𝚘𝚠𝚜 𝚄𝚗𝚔𝚗𝚘𝚠𝚗𝙲𝚖𝚍𝙴𝚡𝚌𝚎𝚙𝚝𝚒𝚘𝚗
– parse the command and set new 𝚛𝚎𝚜𝚞𝚕𝚝
. If command is not valid, throws an 𝚄𝚗𝚔𝚗𝚘𝚠𝚗𝙲𝚖𝚍𝙴𝚡𝚌𝚎𝚙𝚝𝚒𝚘𝚗
contained the error message.

2. 𝚙𝚞𝚋𝚕𝚒𝚌 𝚂𝚝𝚛𝚒𝚗𝚐 𝚐𝚎𝚝𝙼𝚜𝚐()
– output the message of the 𝚛𝚎𝚜𝚞𝚕𝚝
of this step. The output message depends on the calculation 𝚌𝚘𝚞𝚗𝚝
. The output format is as below:
(1)when starting the calculation, it will return "𝙲𝚊𝚕𝚌𝚞𝚕𝚊𝚝𝚘𝚛 𝚒𝚜 𝚘𝚗. 𝚁𝚎𝚜𝚞𝚕𝚝 = [𝚛𝚎𝚜𝚞𝚕𝚝]
"
(2)when calculation 𝚌𝚘𝚞𝚗𝚝
=
1
, it will return "𝚁𝚎𝚜𝚞𝚕𝚝 [𝚘𝚙𝚎𝚛𝚊𝚝𝚘𝚛] [𝚟𝚊𝚕𝚞𝚎] = [𝚛𝚎𝚜𝚞𝚕𝚝]. 𝙽𝚎𝚠 𝚛𝚎𝚜𝚞𝚕𝚝 = [𝚛𝚎𝚜𝚞𝚕𝚝]
"
(3)when calculation 𝚌𝚘𝚞𝚗𝚝
>
1
, it will return "𝚁𝚎𝚜𝚞𝚕𝚝 [𝚘𝚙𝚎𝚛𝚊𝚝𝚘𝚛] [𝚟𝚊𝚕𝚞𝚎] = [𝚛𝚎𝚜𝚞𝚕𝚝]. 𝚄𝚙𝚍𝚊𝚝𝚎𝚍 𝚛𝚎𝚜𝚞𝚕𝚝 = [𝚛𝚎𝚜𝚞𝚕𝚝]
"
(4)when the calculation finish, it will return "𝙵𝚒𝚗𝚊𝚕 𝚛𝚎𝚜𝚞𝚕𝚝 = [𝚛𝚎𝚜𝚞𝚕𝚝]
"

3. 𝚙𝚞𝚋𝚕𝚒𝚌 𝚋𝚘𝚘𝚕𝚎𝚊𝚗 𝚎𝚗𝚍𝙲𝚊𝚕𝚌(𝚂𝚝𝚛𝚒𝚗𝚐 𝚌𝚖𝚍)
– determine whether calculation comes to the end or not.
*/
