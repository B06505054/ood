
public class Pizza {
	private String _size;
	private int _cheeze;
	private int _pepp;
	private int _ham;
	public Pizza() {
		_size="small";
		_cheeze=1;
		_pepp=1;
		_ham=1;
	}
	public Pizza(String size,int cheeze,int pepp,int ham){//initialize
		_size=size;
		_cheeze=cheeze;
		_pepp=pepp;
		_ham=ham;
	}
	public String getSize() {
		return _size;
	}
	public int getNumberOfCheese() {
		return _cheeze;
	}
	public int getNumberOfPepperoni() {
		return _pepp;
	}
	public int getNumberOfHam() {
		return _ham;
	}
	public void setSize(String size) {
		_size=size;
	}
	public void setNumberOfCheese(int cheeze) {
		_cheeze=cheeze;
	}
	public void setNumberOfPepperoni(int pepp) {
		_pepp=pepp;
	}
	public void setNumberOfHam(int ham) {
		_ham=ham;
	}
	
	public double calcCost() {
		double cost;
		if(this._size=="small") {
			cost=10+_cheeze*2+_pepp*2+_ham*2;
		}
		else if(this._size=="medium") {
			cost=12+_cheeze*2+_pepp*2+_ham*2;
		}
		else if(this._size=="large") {
			cost=14+_cheeze*2+_pepp*2+_ham*2;
		}
		else cost=0;
		return cost;
	}
	public boolean equals(Pizza pizza) {
		if(this._size==pizza._size&&this._cheeze==pizza._cheeze&&this._pepp==pizza._pepp&&this._ham==pizza._ham) return true;
		else return false;
	}
	public String toString() {
		return "size = "+_size+", numOfCheese = "+_cheeze+", numOfPepperoni = "+_pepp+", numOfHam = "+_ham;
	}

}
