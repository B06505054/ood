
public class PizzaOrder {
	private Pizza order1,order2,order3;
	private int _num;
	public boolean setNumberPizzas(int num) {
		_num=num;
		return num>=1&&num<=3 ? true:false;
	}
	public void setPizza1(Pizza pizza1) {
		order1=pizza1;
	}
	
	public void setPizza2(Pizza pizza2) {
		if(_num==2) {
			order2=pizza2;
		}else return;
		
	}
	public void setPizza3(Pizza pizza3) {
		if(_num==3) {
			order3=pizza3;
		}else return;
	}
	public double calcTotal() {
		if(_num==1) {
			return order1.calcCost();
		}
		else if(_num==2) {
			return order1.calcCost()+order2.calcCost();
		}
		else if(_num==3) {
			return order1.calcCost()+order2.calcCost()+order3.calcCost();
		}else return 0;
		
	}
}
