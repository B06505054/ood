
public class Circle extends Shape{
	protected double diameter;
	public Circle(double diameter) {
		super(diameter);
		this.diameter=diameter;
	}
	@Override
	public void setLength(double length) {
		this.diameter=length;
	}
	@Override
	public double getArea() {
		return (double)Math.round(100*(diameter/2)*(diameter/2)*Math.PI)/100;
	}
	@Override
	public double getPerimeter() {
		return (double)Math.round(100*diameter*Math.PI)/100;
	}
}