
public class Square extends Shape{
	protected double side_length;
	public Square(double side_length) {
		super(side_length);
		this.side_length=side_length;
	}
	@Override
	public void setLength(double side_length) {
		this.side_length=side_length;
	}
	@Override
	public double getArea() {
		return (double)Math.round(side_length*side_length*100)/100;
	}
	@Override
	public double getPerimeter() {
		return (double)Math.round(side_length*400)/100;
	}
}