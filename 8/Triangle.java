
public class Triangle extends Shape{
	protected double side_length;
	public Triangle(double side_length) {
		super(side_length);
		this.side_length=side_length;
	}
	@Override
	public void setLength(double side_length) {
		this.side_length=side_length;
	}
	@Override
	public double getArea(){
		return (double)Math.round(100*side_length*side_length*Math.sqrt(3)/4)/(double)100;
	}
	@Override
	public double getPerimeter() {
		return (double)Math.round(100*side_length*3)/(double)100;
	}
}