
public class ShapeFactory {
	enum Type {Triangle,Circle,Square};
	public Shape createShape(ShapeFactory.Type shapeType, double length) {
		switch(shapeType) {
		case Triangle:
			Triangle triangle= new Triangle(length);
			return triangle;
		case Circle:
			Circle circle= new Circle(length);
			return circle;
		case Square:
			Square square= new Square(length);
			return square;
		default:
			Triangle shape= new Triangle(length);
			return shape;
		}
	}
	public Shape createShape(ShapeFactory.Type shapeType, int length) {
		switch(shapeType) {
		case Triangle:
			Triangle triangle= new Triangle(length);
			return triangle;
		case Circle:
			Circle circle= new Circle(length);
			return circle;
		case Square:
			Square square= new Square(length);
			return square;
		default:
			Triangle shape= new Triangle(length);
			return shape;
		}
	}
}

